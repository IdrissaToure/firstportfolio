import React, { createContext, useContext, useState } from "react";

const StateContext = createContext()

export const ContextProvider = ({ children }) => {

    const [activeMenu, setActiveMenu] = useState(true);
    const [activeNight, setActiveNight] = useState(false);
    const [screenSize, setScreenSize] = useState(undefined)

    

    return (
        <StateContext.Provider value={{ activeMenu, activeNight, setActiveNight, setActiveMenu, setScreenSize, screenSize }}>
            {children}
        </StateContext.Provider >
    )
}

export const useStateContext = () => useContext(StateContext)