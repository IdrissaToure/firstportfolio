import React, { useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { TooltipComponent } from '@syncfusion/ej2-react-popups';
import './App.css';
import Sidebar from './components/Sidebar';
import Home from './components/Home';
import { useStateContext } from './context/ContextProvider';
import { AiOutlineMenu } from 'react-icons/ai';
import About from './pages/About';
import Services from './pages/Services';
import Portfolio from './pages/Portfolio';
import Contact from './pages/Contact';
import { BsFillMoonFill, BsFillSunFill } from 'react-icons/bs';

const App = () => {

    const sun_style= 'w-72 z-10 bg-slate-300 h-full fixed sidebar dark:bg-secondary-dark-bg';
    const moon_style= 'w-72 z-10 bg-gray-900 h-full fixed sidebar dark:bg-secondary-dark-bg';
    const content_style= 'dark:bg-main-bg flex z-0 items-center bg-slate-200 min-h-screen w-full h-full md:ml-72';
    const ContentMoon_style= 'dark:bg-main-bg flex z-0 items-center bg-gray-800 text-white min-h-screen w-full h-full md:ml-72';

    const { activeNight, setActiveNight, activeMenu, setActiveMenu, screenSize, setScreenSize } = useStateContext();
    useEffect(() => {
        const handleResize = () => setScreenSize(window.innerWidth);

        window.addEventListener('resize', handleResize);

        handleResize();

        return () => window.removeEventListener('resize', handleResize);
    }, []);

    useEffect(() => {
        if (screenSize <= 767) {
            setActiveMenu(false);
        } else {
            setActiveMenu(true);
        }
    }, [screenSize])

    return (
        <div>
            <BrowserRouter>
                <div className='flex relative dark:bg-main-dark-bg'>
                    <div className='fixed right-4 bottom-4' style={{ zIndex: '1000' }}>
                        <TooltipComponent content="Mode" position="Top">
                            <button type='button' onClick={() => setActiveNight((prevActiveMenu) => !prevActiveMenu)} className='text-3xl p-3 hover:drop-shadow-xl hover:bg-light-gray text-gray-600 bg-gray-500 rounded-full'>
                                {activeNight ? (<BsFillMoonFill />) : (<BsFillSunFill />)}
                            </button>
                        </TooltipComponent>
                    </div>
                    {activeMenu ? (<div className={activeNight ? moon_style : sun_style} >
                        <Sidebar />
                    </div>) : (<></>)
                    }
                    <div className={activeNight ? ContentMoon_style : content_style}>
                        <div className='h-full flex p-10 justify-between items-center w-full'>
                            <TooltipComponent content="Menu" position='BottomCenter' className='absolute top-1 left-2'>
                                <button type='button' onClick={() => setActiveMenu((prevActiveMenu) => !prevActiveMenu)} className='text-xl rounded-full p-3 hover:bg-light-gray  mt-4 block md:hidden'>
                                    <AiOutlineMenu />
                                </button>
                            </TooltipComponent>
                            <Routes>
                                {/* Welcome */}
                                <Route path='/' element={<Home />} />

                                {/* Pages */}
                                <Route path='/Home' element={<Home />} />
                                <Route path='/About' element={<About />} />
                                <Route path='/Services' element={<Services />} />
                                <Route path='/Portfolio' element={<Portfolio />} />
                                <Route path='/Contact' element={<Contact />} />
                            </Routes>
                        </div>
                    </div>
                </div>
            </BrowserRouter>
        </div>
    )
}

export default App