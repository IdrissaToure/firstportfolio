import { TooltipComponent } from '@syncfusion/ej2-react-popups';
import React from 'react'
import { NavLink } from 'react-router-dom'
import { links } from '../data/dummy'
import { MdOutlineCancel } from 'react-icons/md';
import { useStateContext } from '../context/ContextProvider';

const Sidebar = () => {

    const { setActiveMenu } = useStateContext();
    const { activeNight } = useStateContext();

    const activeLink = 'flex items-center gap-5 pl-4 pt-3 pb-2.5 rounded-lg text-white text-md m-2 bg-red-500';
    const normalLink = 'flex items-center gap-5 pl-4 pt-3 pb-2.5 rounded-lg text-white text-md text-black dark:text-gary-200 dark:hover:text-black hover:bg-light-gray hover:text-zinc-700 m-2'
    const normalLinkMoon = 'flex items-center gap-5 pl-4 pt-3 pb-2.5 rounded-lg text-white text-md dark:text-gary-200 dark:hover:text-black hover:bg-light-gray hover:text-zinc-700 m-2'
    const title_style="absolute top-20 font-bold text-4xl text-bold"
    const titleMoon_style="absolute top-20 font-bold text-4xl text-bold text-white"

    return (
        <div className='flex h-full w-full items-center justify-center'>
            <h1 className={activeNight ? titleMoon_style : title_style}><span className='text-red-500'>E</span>nux</h1>
            <TooltipComponent content="Menu" position='BottomCenter' className='absolute top-2 right-2'>
              <button type='button' onClick={() => setActiveMenu((prevActiveMenu)=>!prevActiveMenu)} className='text-xl rounded-full p-3 hover:bg-light-gray  mt-4 block md:hidden'>
                <MdOutlineCancel />
              </button>
            </TooltipComponent>
            <div className='w-full'>
                {links.map((link) => (
                    <NavLink to={`/${link.name}`} key={link.name} className={({ isActive }) => isActive ? activeLink : activeNight ? normalLinkMoon : normalLink }>
                        {link.icon}
                        <span className='capitalize'>{link.name}</span>
                    </NavLink>
                ))}
            </div>
        </div>
    )
}

export default Sidebar