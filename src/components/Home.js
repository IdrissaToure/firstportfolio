import React from 'react';
import profile from "../data/Images/avatar.jpg"

const Home = () => {
  return (
    <>
      <div className='home-text flex-row'>
        <h1 className='font-bold text-4xl '>Hello, my name is <span className='text-red-500'>Enux Assir</span></h1>
        <h1 className='font-bold text-2xl '>I'm a <span>Ing. DevSecOps</span></h1>
        <p>I'm a ing. DevSecOps with extensive experience for over 2 years. My expertise is to create and configure a CI/CD pipiline, and many more</p>
      </div>
      <div className='w-3/5 home flex-row'>
        <img className='w-2/3 rounded border-separate m-3 border-red-600 shadow-lg' src={profile} />
      </div>
    </>
  )
}

export default Home