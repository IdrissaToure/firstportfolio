import React from 'react'

const Portfolio = () => {
    return (
        <div className='w-full h-full'>
            <h1 className='fixed font-bold text-3xl'><span className='text-red-600'>P</span>ortfolio</h1>
            <div class="grid grid-cols-3 gap-3 h-6/12 mt-10 overflow-auto">
                <div className='bg-red-300 rounded h-56'>01</div>
                <div className='bg-red-300 rounded h-56'>01</div>
                <div className='bg-red-300 rounded h-56'>01</div>
                <div className='bg-red-300 rounded h-56'>01</div>
                <div className='bg-red-300 rounded h-56'>01</div>
                <div className='bg-red-300 rounded h-56'>01</div>
                <div className='bg-red-300 rounded h-56'>01</div>
                <div className='bg-red-300 rounded h-56'>01</div>
                <div className='bg-red-300 rounded h-56'>01</div>
                <div className='bg-red-300 rounded h-56'>09</div>
            </div>
        </div>
    )
}

export default Portfolio