import React from 'react'
import { AiFillGitlab, AiFillLinkedin, AiFillRocket, AiOutlineFacebook, AiOutlineInstagram, AiOutlineTwitter } from 'react-icons/ai'
import { NavLink } from 'react-router-dom'
import profile from "../data/Images/avatar.jpg"

const Services = () => {
    return (
        <div className='w-full compo-service flex justify-between'>
            <div className='service-comp p-3 flex items-center'>
                <div className='w-full'>
                    <div>
                        <img className='ml-auto mr-auto w-40 h-40 rounded-full' src={profile} />
                    </div>
                    <div className='w-full text-center mb-4 pt-2'>
                        <h3 className='font-bold'>Enux Assir</h3>
                        <p>Ing. CyberSecurity</p>
                        <p className='mb-4'>Full Stack Developer</p>
                        <NavLink to="/" className='inline p-2 bg-red-600 rounded mt-2'>Curriculum Vitae</NavLink>
                    </div>
                    <div className='w-full mb-3'>
                        <div className='flex border-b-1 mt-1 border-black justify-between pb-1'>
                            <p className='flex'><AiFillRocket className='mr-2 text-2xl' /> Website</p>
                            <NavLink to='/'>website name</NavLink>
                        </div>
                        <div className='flex border-b-1 mt-1 border-black justify-between pb-1'>
                            <p className='flex'><AiFillGitlab className='mr-2 text-2xl' /> Gitlab</p>
                            <NavLink to='/'>Gitlab username</NavLink>
                        </div>
                        <div className='flex border-b-1 mt-1 border-black justify-between pb-1'>
                            <p className='flex'><AiFillLinkedin className='mr-2 text-2xl' /> Linkedin</p>
                            <NavLink to='/'>name</NavLink>
                        </div>
                        <div className='flex border-b-1 mt-1 border-black justify-between pb-1'>
                            <p className='flex'><AiOutlineTwitter className='mr-2 text-2xl' />Twitter</p>
                            <NavLink to='/'>username</NavLink>
                        </div>
                        <div className='flex border-b-1 mt-1 border-black justify-between pb-1'>
                            <p className='flex'><AiOutlineInstagram className='mr-2 text-2xl' />Instagram</p>
                            <NavLink to='/'>username</NavLink>
                        </div>
                        <div className='flex border-b-1 mt-1 border-black justify-between pb-1'>
                            <p className='flex'><AiOutlineFacebook className='mr-2 text-2xl' />Facebook</p>
                            <NavLink to='/'>account fb</NavLink>
                        </div>
                    </div>
                </div>
            </div>
            <div className='service-comp p-3 items-center flex'>
                <div className='w-full'>
                    <div className='w-full mb-6'>
                        <div className='flex border-b-1 mt-1 border-black justify-between pb-1'>
                            <p>Full Name</p>
                            <p>Enux Assir</p>
                        </div>
                        <div className='flex border-b-1 mt-1 border-black justify-between pb-1'>
                            <p>Email</p>
                            <p>Enuxassir@gmail.com</p>
                        </div>
                        <div className='flex border-b-1 mt-1 border-black justify-between pb-1'>
                            <p>Phone</p>
                            <p>(+00) 1234567890</p>
                        </div>
                        <div className='flex border-b-1 mt-1 border-black justify-between pb-1'>
                            <p>Address</p>
                            <p>France,FR</p>
                        </div>
                    </div>
                    <h4 className='mb-2'><span className='text-red-600' style={{ fontStyle: "italic" }}>assignment</span> Project Status</h4>
                    <div className="mb-1 text-base font-medium dark:text-white">Web Design</div>
                    <div className="w-full bg-gray-400 rounded-full h-2.5 dark:bg-gray-700">
                        <div className="bg-red-600 h-2.5 rounded-full" style={{ width: "70%" }}></div>
                    </div>
                    <div className="mb-1 text-base font-medium dark:text-white">Dev Mobile</div>
                    <div className="w-full bg-gray-400 rounded-full h-2.5 dark:bg-gray-700">
                        <div className="bg-red-600 h-2.5 rounded-full" style={{ width: "70%" }}></div>
                    </div>
                    <div className="mb-1 text-base font-medium dark:text-white">Dev API</div>
                    <div className="w-full bg-gray-400 rounded-full h-2.5 dark:bg-gray-700">
                        <div className="bg-red-600 h-2.5 rounded-full" style={{ width: "70%" }}></div>
                    </div>
                    <div className="mb-1 text-base font-medium dark:text-white">Website Markup</div>
                    <div className="w-full bg-gray-400 rounded-full h-2.5 dark:bg-gray-700">
                        <div className="bg-red-600 h-2.5 rounded-full" style={{ width: "70%" }}></div>
                    </div>
                    <div className="mb-1 text-base font-medium dark:text-white">Gouvernance Security</div>
                    <div className="w-full bg-gray-400 rounded-full h-2.5 dark:bg-gray-700">
                        <div className="bg-red-600 h-2.5 rounded-full" style={{ width: "70%" }}></div>
                    </div>
                    <div className="mb-1 text-base font-medium dark:text-white">Pentest</div>
                    <div className="w-full bg-gray-400 rounded-full h-2.5 dark:bg-gray-700">
                        <div className="bg-red-600 h-2.5 rounded-full" style={{ width: "70%" }}></div>
                    </div>
                    <div className="mb-1 text-base font-medium dark:text-white">Digital Marketing</div>
                    <div className="w-full bg-gray-400 rounded-full h-2.5 dark:bg-gray-700">
                        <div className="bg-red-600 h-2.5 rounded-full" style={{ width: "70%" }}></div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Services