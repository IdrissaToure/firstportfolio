import React from 'react'

const Contact = () => {
    return (
        <>
            <div class="max-w-screen-md mx-auto p-5">
                <div class="text-center mb-16">
                    <h3 class="text-3xl sm:text-4xl leading-normal font-extrabold tracking-tight "><span class="text-red-600">C</span>ontact
                    </h3>
                </div>

                <form class="w-full">
                    <div class="flex flex-wrap -mx-3 mb-6">
                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <input class="appearance-none block w-full bg-gray-100 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="grid-first-name" type="text" placeholder="Your firstname" />
                        </div>
                        <div class="w-full md:w-1/2 px-3">
                            <input class="appearance-none block w-full bg-gray-100 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-last-name" type="text" placeholder="Your lastname" />
                        </div>
                    </div>
                    <div class="flex flex-wrap -mx-3 mb-6">
                        <div class="w-full px-3">
                            <input class="appearance-none block w-full bg-gray-100 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-email" type="email" placeholder="email address" />
                        </div>
                    </div>

                    <div class="flex flex-wrap -mx-3 mb-6">
                        <div class="w-full px-3">
                            <textarea rows="10" class="appearance-none block w-full bg-gray-100 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" placeholder='your message'></textarea>
                        </div>
                        <div class="flex justify-between w-full px-3">
                            <div class="md:flex md:items-center">
                                <label class="block text-gray-500 font-bold">
                                    <input class="mr-2 leading-tight" type="checkbox" />
                                    <span class="text-sm">
                                        Send me your newsletter!
                                    </span>
                                </label>
                            </div>
                            <button class="shadow bg-indigo-600 hover:bg-indigo-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-6 rounded" type="submit">
                                Send Message
                            </button>
                        </div>

                    </div>

                </form>
            </div>
        </>
    )
}

export default Contact