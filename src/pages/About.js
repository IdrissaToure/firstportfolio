import React from 'react'
import { AiFillLock } from 'react-icons/ai'
import { BiMobileAlt } from 'react-icons/bi'
import { BsFillShieldLockFill } from 'react-icons/bs'
import { CgWebsite } from 'react-icons/cg'
import hacker from "../data/Images/service.jpg"


const About = () => {
    return (
        <div className='w-full services h-full flex justify-around items-center'>
            <div className='h-full p-2 service-video service-comp items-center '>
                <img src={hacker} className='rounded shadow-lg' />
            </div>
            <div className='p-2 service-comp h-full flex-row  items-center'>
                <h1 className='mb-3 font-bold text-3xl'>About <span className='text-red-600'>Me</span></h1>
                <h3 className='font-bold text-2xl'>My mission is to</h3>
                <h3 className='font-bold mb-4 text-2xl'>make your life easier.</h3>
                <p>I am a Ing. in CyberSecurity. Excellent knowledge of good business practices, regulatory standards and optimal control systems. Proficient in applying knowledge of trends and patterns to drive meaningful improvements in cybersecurity</p>
                <div className='w-full flex flex-wrap  justify-around'>
                    <button className="bg-gray-100 hover:bg-red-400 m-2 pl-1 shadow w-5/12 text-gray-800 font-bold py-2 px-1 rounded inline-flex items-center">
                        <AiFillLock className="fill-current w-4 h-4 mr-2"/>
                        <span>Security </span>
                    </button>
                    <button className="bg-gray-100 hover:bg-red-400 m-2 pl-1 shadow w-5/12 text-gray-800 font-bold py-2 px-1 rounded inline-flex items-center">
                        <BsFillShieldLockFill className="fill-current w-4 h-4 mr-2" />
                        <span>Pentesting</span>
                    </button>
                    <button className="bg-gray-100 hover:bg-red-400 m-2 pl-1 shadow w-5/12 text-gray-800 font-bold py-2 px-1 rounded inline-flex items-center">
                        <CgWebsite className="fill-current w-4 h-4 mr-2"/>
                        <span>Dev Web</span>
                    </button>
                    <button className="bg-gray-100 hover:bg-red-400 m-2 pl-1 shadow w-5/12 text-gray-800 font-bold py-2 px-1 rounded inline-flex items-center">
                        <BiMobileAlt className="fill-current w-4 h-4 mr-2" />
                        <span>Dev Mobile</span>
                    </button>
                </div>

            </div>
        </div>
    )
}

export default About