import React from 'react';
import { AiFillHome, AiOutlineUser, AiFillContacts } from 'react-icons/ai';
import { FiStar, FiImage, FiList } from 'react-icons/fi';

export const links= [
    {
      name: 'Home',
      icon: <AiFillHome />,
    },
    {
      name: 'About',
      icon: <AiOutlineUser />,
    },
    {
      name: 'Services',
      icon: <FiList />,
    },
    {
      name: 'Portfolio',
      icon: <FiImage />,
    },
    {
      name: 'Contact',
      icon: <AiFillContacts />,
    },
  ]